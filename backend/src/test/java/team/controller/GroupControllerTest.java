package team.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import team.api.group.Group;
import team.api.group.GroupForm;
import team.api.group.GroupService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class GroupControllerTest {

    public static final String GROUP_ID = "groupId";
    public static final String USER_ID = "userId";
    @Mock
    private GroupService delegate;

    @Mock
    private Group groupMock;

    @Mock
    private Group groupMock2;

    @Mock
    private GroupForm groupForm;

    @InjectMocks
    private GroupController groupController;

    private String expected;

    private List<Group> groupList = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        when(delegate.getAllGroups()).thenReturn(groupList);
        when(delegate.getGroupById(GROUP_ID)).thenReturn(groupMock);
        when(delegate.createGroup(groupForm)).thenReturn(groupMock);
        when(delegate.deleteGroupById(GROUP_ID)).thenReturn("DELETED");
        when(delegate.updateGroup(groupMock)).thenReturn(groupMock2);
        when(delegate.getGroupByUserId(USER_ID)).thenReturn(groupList);
    }

    @Test
    public void getAllGroups() {
        List<Group> list = groupController.getAllGroups();
        assertEquals(list, groupList);
    }

    @Test
    public void getGroupById() {
        Group group = groupController.getGroupById(GROUP_ID);
        assertEquals(group, groupMock);
    }

    @Test
    public void createGroup() {
        Group group = groupController.createGroup(groupForm);
        assertEquals(group, groupMock);
    }

    @Test
    public void deleteGroupById() {
        expected = groupController.deleteGroupById(GROUP_ID);
        assertEquals("DELETED", expected);
    }

    @Test
    public void updateGroup() {
        Group group = groupController.updateGroup(groupMock);
        assertEquals(group, groupMock2);
    }

    @Test
    public void getGroupByUserId() {
        List<Group> list = groupController.getGroupByUserId(USER_ID);
        assertEquals(list, groupList);
    }

    @Test
    public void putUserInGroup() {
        expected = groupController.putUserInGroup(USER_ID, GROUP_ID);
        assertEquals("DONE", expected);
    }
}