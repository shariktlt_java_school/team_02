package team.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import team.api.user.User;
import team.api.user.UserForm;
import team.api.user.UserService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class UserControllerTest {

    public static final String USER_ID = "userId";
    public static final String GROUP_ID = "groupId";
    @Mock
    private UserService delegate;

    @Mock
    private UserForm userForm;

    @Mock
    private User userMock;

    @Mock
    private User userMock2;

    @InjectMocks
    private UserController userController;

    List<User> userList = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        when(delegate.getUserById(USER_ID)).thenReturn(userMock);
        when(delegate.deleteUserById(USER_ID)).thenReturn("DELETED");
        when(delegate.createUser(userForm)).thenReturn(userMock);
        when(delegate.getAllUsers()).thenReturn(userList);
        when(delegate.updateUser(userMock)).thenReturn(userMock2);
        when(delegate.getUsersByGroupId(GROUP_ID)).thenReturn(userList);

    }

    @Test
    public void getUserById() {
        User user = userController.getUserById(USER_ID);
        assertEquals(user, userMock);
    }

    @Test
    public void deleteUserById() {
        String expected = userController.deleteUserById(USER_ID);
        assertEquals("DELETED", expected);
    }

    @Test
    public void createUser() {
        User user = userController.createUser(userForm);
        assertEquals(user, userMock);
    }

    @Test
    public void getAllUsers() {
        List<User> list = userController.getAllUsers();
        assertEquals(list, userList);
    }

    @Test
    public void updateUser() {
        User user = userController.updateUser(userMock);
        assertEquals(user, userMock2);
    }

    @Test
    public void getUsersByGroupId() {
        List<User> list = userController.getUsersByGroupId(GROUP_ID);
        assertEquals(list, userList);
    }

    @Test
    public void putUserInGroup() {
    }
}