package team.app;

import org.junit.Test;

import static org.junit.Assert.*;

public class IndexControllerTest {

    @Test
    public void index() {
        IndexController indexController = new IndexController();
        String expected = indexController.index();
        assertEquals("index", expected);
    }
}