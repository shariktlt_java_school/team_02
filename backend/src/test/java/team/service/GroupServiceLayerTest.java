package team.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import team.api.group.Group;
import team.api.group.GroupForm;
import team.da.GroupDALayer;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class GroupServiceLayerTest {

    public static final String GROUP_ID = "groupId";
    public static final String USER_ID = "userId";
    private List<Group> groupList = new ArrayList<>();

    @Mock
    private Group groupMock;

    @Mock
    private Group groupMock2;

    @Mock
    private GroupForm groupForm;

    @Mock
    private GroupDALayer groupDALayer;

    @InjectMocks
    GroupServiceLayer groupServiceLayer;

    private String expected;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        when(groupDALayer.getAllGroups()).thenReturn(groupList);
        when(groupDALayer.getGroupById(GROUP_ID)).thenReturn(groupMock);
        when(groupDALayer.createGroup(groupForm)).thenReturn(groupMock);
        when(groupDALayer.updateGroupById(groupMock)).thenReturn(groupMock2);
        when(groupDALayer.getGroupsByPkUserId(USER_ID)).thenReturn(groupList);
    }

    @Test
    public void getAllGroups() {
        List<Group> list = groupServiceLayer.getAllGroups();
        assertEquals(list, groupList);
    }

    @Test
    public void getGroupById() {
        Group group = groupServiceLayer.getGroupById(GROUP_ID);
        assertEquals(group, groupMock);
    }

    @Test
    public void createGroup() {
        Group group = groupServiceLayer.createGroup(groupForm);
        assertEquals(group, groupMock);
    }

    @Test
    public void deleteGroupById() {
        expected = groupServiceLayer.deleteGroupById(GROUP_ID);
        assertEquals("DELETED", expected);
    }

    @Test
    public void updateGroup() {
        Group group = groupServiceLayer.updateGroup(groupMock);
        assertEquals(group, groupMock2);
    }

    @Test
    public void getGroupByUserId() {
        List<Group> list = groupServiceLayer.getGroupByUserId(USER_ID);
        assertEquals(list, groupList);
    }

    @Test
    public void putUserInGroup() {
    expected = groupServiceLayer.putUserInGroup(USER_ID, GROUP_ID);
    assertEquals(expected, "DONE");
    }

    @Test
    public void link() {
        List<String>list = new ArrayList<>();
        expected = groupServiceLayer.link(USER_ID, list);
        assertEquals("linking done", expected);
    }
}