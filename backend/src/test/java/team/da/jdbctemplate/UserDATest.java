package team.da.jdbctemplate;

import lombok.SneakyThrows;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import team.api.user.User;

import java.sql.ResultSet;
import java.sql.Savepoint;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;
import static team.da.jdbctemplate.UserDA.*;

public class UserDATest {


    public static final String USER_ID = "userId";
    public static final String GROUP_ID = "groupID";

    @Mock
    User userMock;

    @Mock
    private JdbcTemplate jdbcTemplate;

    @InjectMocks
    private UserDA da;

    @Before
    public void setUp() throws Exception {
        openMocks(this);

    }

    @Test
    public void setJdbcInsert() {
    }

    @Test
    @SneakyThrows
    public void getUserById() {

//тоже не понимаю

        ResultSet resultSetMock = mock(ResultSet.class);
        List <User> expectedResult = new ArrayList();

        when(resultSetMock.getString("user_id")).thenReturn(USER_ID);
        when(resultSetMock.getString("first_name")).thenReturn("firstName");
        when(resultSetMock.getString("last_name")).thenReturn("lastName");
        when(resultSetMock.getString("birth_date")).thenReturn("birthDate");
        when(resultSetMock.getString("phone")).thenReturn("phone");
        when(resultSetMock.getString("email")).thenReturn("email");
        when(resultSetMock.getString("role")).thenReturn("role");


        when(jdbcTemplate.query(eq(QUERY_SELECT_BY_ID), any(RowMapper.class), eq(USER_ID))).thenAnswer(invocation -> {

            RowMapper<User> rowMapper = invocation.getArgument(1, RowMapper.class);
            expectedResult.add(rowMapper.mapRow(resultSetMock, 1));
            return expectedResult;
        });

        User user = da.getUserById(USER_ID);


        assertEquals( 0, expectedResult.size());

    }

    @Test
    public void deleteUserById() {
    }

    @Test
    public void createUser() {

    }

    @Test
    @SneakyThrows
    public void getAllUsers() {
        ResultSet resultSetMock = mock(ResultSet.class);
        List <User> expectedResult = new ArrayList();

        when(resultSetMock.getString("user_id")).thenReturn(USER_ID);
        when(resultSetMock.getString("first_name")).thenReturn("firstName");
        when(resultSetMock.getString("last_name")).thenReturn("lastName");
        when(resultSetMock.getString("birth_date")).thenReturn("birthDate");
        when(resultSetMock.getString("phone")).thenReturn("phone");
        when(resultSetMock.getString("email")).thenReturn("email");
        when(resultSetMock.getString("role")).thenReturn("role");


        when(jdbcTemplate.query(eq(SELECT_ALL_USERS), any(RowMapper.class))).thenAnswer(invocation -> {

            RowMapper<User> rowMapper = invocation.getArgument(1, RowMapper.class);
            expectedResult.add(rowMapper.mapRow(resultSetMock, 1));
            return expectedResult;
        });

        List<User> list = da.getAllUsers();
        User user = list.get(0);
        assertEquals(USER_ID, user.getUserId());
        assertEquals(1, list.size());
        assertEquals(list, expectedResult);
    }

    @Test
    public void updateUser() {
    }

    @Test
    @SneakyThrows
    public void getUsersByPkGroupId() {

    }

    @Test
    public void putUserInGroup() {
    }

    @Test
    public void linkUser() {
    }
}