package team.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import team.api.user.User;
import team.api.user.UserForm;
import team.da.jpa.converter.UserMapper;
import team.da.jpa.entity.GroupEntity;
import team.da.jpa.entity.GroupLinkEntity;
import team.da.jpa.entity.UserEntity;
import team.da.jpa.repository.EntrantEntityRepository;
import team.da.jpa.repository.GroupEntityRepository;
import team.da.jpa.repository.GroupLinkEntityRepository;
import team.da.jpa.repository.UserEntityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class JPAUserDATest {

    public static final String USER_ID = "userID";
    public static final String GROUP_ID = "groupId";
    @Mock
    private UserEntityRepository userRepo;

    @Mock
    private GroupLinkEntityRepository linkRepo;

    @Mock
    private GroupEntityRepository groupRepo;

    @Mock
    private EntrantEntityRepository entrantRepo;

    @Mock
    private UserMapper userMapper;

    @Mock
    private UserEntity userEntity;

    @Mock
    private User userMock;

    @Mock
    private UserForm userForm;

    private List<GroupLinkEntity> groupLinkEntities = new ArrayList<>();

    private final List<User> userList = new ArrayList<>();
    private final List<UserEntity> userEntities = new ArrayList<>();

    private final UserEntity entity = new UserEntity();
    private final GroupEntity groupEntity = new GroupEntity();
    private final Optional<GroupEntity> groupEntityOptional = Optional.of(groupEntity);
    private final Optional<UserEntity> userEntityOptional = Optional.of(entity);
    private final Optional<UserEntity> userEntityOptional2 = Optional.ofNullable(userEntity);

    String expected;

    @InjectMocks
    private JPAUserDA jpaUserDA;

    @Before
    public void setUp() throws Exception {

        openMocks(this);
        when(userRepo.findAll()).thenReturn(userEntities);
        when(userMapper.map(entity)).thenReturn(userMock);
        when(groupRepo.findById(GROUP_ID)).thenReturn(groupEntityOptional);
        when(userRepo.findById(USER_ID)).thenReturn(userEntityOptional);

    }

    @Test
    public void linkUser() {
        expected = jpaUserDA.linkUser(USER_ID, GROUP_ID);
        assertEquals("Link added", expected);
    }

    @Test
    public void getUserById() {
        User user = jpaUserDA.getUserById(USER_ID);
        assertEquals(userMock, user);
    }

    @Test
    public void deleteUserById() {

        expected = jpaUserDA.deleteUserById(USER_ID);
        assertEquals("DELETED", expected);
    }

    @Test (expected = RuntimeException.class)
    public void deleteUserByIdNull() {
        when(userRepo.findById(USER_ID)).thenReturn(userEntityOptional2);
        expected = jpaUserDA.deleteUserById(USER_ID);
    }


    @Test
    public void createUser() {
        when(userForm.getFirstName()).thenReturn("firstName");
        when(userForm.getLastName()).thenReturn("lastName");
        User user = jpaUserDA.createUser(userForm);
        assertEquals("firstName", user.getFirstName());
        assertNotNull(user.getUserId());
    }

    @Test
    public void getAllUsers() {
        when(userMapper.mapList(userEntities)).thenReturn(userList);
        List<User> list = jpaUserDA.getAllUsers();
        assertEquals(list, userList);
    }

    @Test
    public void updateUser() {
        when(userMapper.map(userMock)).thenReturn(userEntity);
        User user = jpaUserDA.updateUser(userMock);
        assertEquals(user, userMock);
    }

    @Test
    public void getUsersByPkGroupId() {

        // Здесь я совсем не понимаю что делать.

        List<User> list = jpaUserDA.getUsersByPkGroupId(GROUP_ID);
        assertEquals(list, userList);
    }

    @Test
    public void putUserInGroup() {
        // И здесь
        jpaUserDA.putUserInGroup(USER_ID, GROUP_ID);

    }
}