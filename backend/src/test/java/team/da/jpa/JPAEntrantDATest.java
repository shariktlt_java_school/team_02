package team.da.jpa;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import team.api.entrant.EntrantInfo;
import team.da.jpa.converter.EntrantInfoMapper;
import team.da.jpa.entity.EntrantEntity;
import team.da.jpa.repository.EntrantEntityRepository;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class JPAEntrantDATest {

    public static final String USERNAME = "username";
    @Mock
    private EntrantEntityRepository entrantRepo;

    @Mock
    private EntrantInfoMapper mapper;

    @Mock
    private EntrantEntity entityMock;

    @Mock
    private EntrantInfo entrantInfoMock;

    @InjectMocks
    private JPAEntrantDA jpaEntrantDA;


    @Before
    public void setUp() throws Exception {
        openMocks(this);
        when(mapper.map(entrantInfoMock)).thenReturn(entityMock);
        when(entrantRepo.save(entityMock)).thenReturn(entityMock);
        when(entrantRepo.findByUsername(USERNAME)).thenReturn(entityMock);
        when(mapper.map(entityMock)).thenReturn(entrantInfoMock);
    }

    @Test
    public void register() {
        EntrantInfo eInfo = jpaEntrantDA.register(entrantInfoMock);
        assertEquals(entrantInfoMock, eInfo);
    }

    @Test
    public void findByUsername() {
        EntrantInfo eInfo = jpaEntrantDA.findByUsername(USERNAME);
        assertEquals(eInfo, entrantInfoMock);

    }
}