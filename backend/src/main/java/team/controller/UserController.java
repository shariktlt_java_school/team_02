package team.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import team.api.user.User;
import team.api.user.UserForm;
import team.api.user.UserService;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController implements UserService {

    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private UserService delegate;


    /**
     * Получение пользователя по ID.
     *
     * @param userId
     * @return User
     */
    @Override
    @GetMapping("/getUserById/{id}")
    public User getUserById(@PathVariable("id") final String userId) {
        return delegate.getUserById(userId);
    }

    /**
     * Удаление пользователя по ID.
     *
     * @param userId
     * @return String
     */
    @Override
    @GetMapping("/deleteUserById/{id}")
    public String deleteUserById(@PathVariable("id") final String userId) {
        return delegate.deleteUserById(userId);
    }

    /**
     * Создание пользователя.
     * по умолчанию - студент.
     *
     * @param form
     * @return User
     */
    @Override
    @PostMapping("/createUser")
    public User createUser(@RequestBody final UserForm form) {
        return delegate.createUser(form);
    }

    /**
     * Получение всех пользователей.
     *
     * @return List User
     */
    @Override
    @GetMapping("/getAllUsers")
    public List<User> getAllUsers() {
        return delegate.getAllUsers();
    }

    /**
     * Обновление информации о пользователе.
     *
     * @param user
     * @return User
     */
    @Override
    @PostMapping("/updateUser")
    public User updateUser(@RequestBody final User user) {
        return delegate.updateUser(user);
    }

    /**
     * Получение списка пользователей по Id группы.
     *
     * @param groupId
     * @return UsersList
     */
    @Override
    @GetMapping("/getUsersByGroupId/{id}")
    public List<User> getUsersByGroupId(final @PathVariable("id")  String groupId) {
        return delegate.getUsersByGroupId(groupId);
    }

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     */
    @Override
    @GetMapping ("/putUserInGroup/{userId}/{groupId}")
    public void putUserInGroup(final  @PathVariable ("userId") String userId,
                               @PathVariable ("groupId") final String groupId) {
        delegate.putUserInGroup(userId, groupId);
    }
}
