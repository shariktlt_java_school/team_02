package team.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import team.api.group.Group;
import team.api.group.GroupForm;
import team.api.group.GroupService;

import java.util.List;

@RestController
@RequestMapping("/group")
public class GroupController implements GroupService {


    /**
     * Делегат для передачи вызова.
     */
    @Autowired
    private GroupService delegate;


    /**
     * Получение всех доступных групп.
     *
     * @return List
     */
    @Override
    @GetMapping("/getAllGroups")
    public List<Group> getAllGroups() {
        return delegate.getAllGroups();
    }

    /**
     * Получение группы по Id.
     *
     * @param groupId
     * @return Group
     */
    @Override
    @GetMapping("/getGroupById/{id}")
    public Group getGroupById(@PathVariable("id") final String groupId) {
        return delegate.getGroupById(groupId);
    }

    /**
     * Создание группы.
     *
     * @param groupForm
     * @return Group
     */
    @Override
    @PostMapping("/createGroup")
    public Group createGroup(@RequestBody final GroupForm groupForm) {
        return delegate.createGroup(groupForm);
    }

    /**
     * Удаление группы по Id.
     *
     * @param groupId
     * @return Group
     */
    @Override
    @GetMapping("/deleteGroupById/{id}")
    public String deleteGroupById(@PathVariable("id") final String groupId) {
        return delegate.deleteGroupById(groupId);
    }

    /**
     * Обновление информации о группе.
     *
     * @param group
     * @return Group
     */
    @Override
    @PostMapping("/updateGroup")
    public Group updateGroup(@RequestBody final Group group) {
        return delegate.updateGroup(group);
    }

    /**
     * получить список групп в которых состоит пользователь.
     *
     * @param userId
     * @return GroupList
     */
    @Override
    @GetMapping("/getGroupByUserId/{id}")
    public List<Group> getGroupByUserId(final @PathVariable("id") String userId) {
        return delegate.getGroupByUserId(userId);
    }

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     * @return message
     */
    @Override
    @GetMapping("/putUserInGroup/{userId}/{groupId}")
    public String putUserInGroup(final @PathVariable("userId") String userId,
                               @PathVariable("groupId") final String groupId) {
        delegate.putUserInGroup(userId, groupId);
        return "DONE";
    }

}
