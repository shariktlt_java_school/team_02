package team.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import team.api.entrant.EntrantInfo;
import team.api.entrant.EntrantService;
import team.da.EntrantDALayer;

import static java.util.Optional.ofNullable;

@RestController
@RequestMapping("/entrant")
public class EntrantController implements EntrantService {

    /**
     * Пустой пользователь.
     */
    public static final EntrantInfo EMPTY_ENTRANT = EntrantInfo.builder().id("N").build();

    /**
     * Зависимость на DA слой.
     */
    @Autowired
    private EntrantDALayer entrantServiceDA;

    /**
     * Регистрация пользователя.
     *
     * @param entrantInfo
     * @return id
     */
    @Override
    @PostMapping("/register")
    public String register(@RequestBody final EntrantInfo entrantInfo) {
        EntrantInfo entrant = entrantServiceDA.register(entrantInfo);

        if (entrant == null) {
            throw new IllegalStateException("Не удалось зарегистрировать пользователя");
        }

        return entrant.getId();
    }

    /**
     * Получение данных о пользователе.
     *
     * @param username
     * @return username
     */
    @Override
    @GetMapping("/loadUserByUsername/{username}")
    public EntrantInfo loadUserByUsername(@PathVariable("username") final String username) {
        return ofNullable(entrantServiceDA.findByUsername(username)).orElse(EMPTY_ENTRANT);
    }
}
