package team.da;

import team.api.common.AcceptorArgument;
import team.api.group.Group;
import team.api.group.GroupForm;

import java.util.List;

public interface GroupDALayer {


    /**
     * Связывание пользователя и группы.
     *
     * @param userId
     * @param groupId
     * @return message
     */
    String linkUser(String userId, String groupId);

    /**
     * Получение всех доступных групп.
     *
     * @return List
     */
    List<Group> getAllGroups();

    /**
     * Получение группы по Id.
     *
     * @param groupId
     * @return Group
     */
    Group getGroupById(String groupId);

    /**
     * Создание группы.
     *
     * @param groupForm
     * @return Group
     */
    @AcceptorArgument
    Group createGroup(GroupForm groupForm);

    /**
     * Удаление группы по Id.
     *
     * @param groupId
     * @return Group
     */
    String deleteGroupById(String groupId);

    /**
     * Обновление информации о группе.
     *
     * @param group
     * @return Group
     */
    Group updateGroupById(Group group);

    /**
     * получить список групп в которых состоит пользователь.
     *
     * @param userId
     * @return GroupList
     */
    List<Group> getGroupsByPkUserId(String userId);

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     */
    void putUserInGroup(String userId, String groupId);

}
