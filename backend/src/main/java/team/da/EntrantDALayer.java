package team.da;

import team.api.entrant.EntrantInfo;

public interface EntrantDALayer {

    /**
     * Регистрация пользователя.
     * @param entrantInfo
     * @return entrantInfo
     */
    EntrantInfo register(EntrantInfo entrantInfo);

    /**
     * Поиск пользователя.
     * @param username
     * @return entrantInfo
     */
    EntrantInfo findByUsername(String username);
}
