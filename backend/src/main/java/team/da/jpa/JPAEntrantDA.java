package team.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import team.api.entrant.EntrantInfo;
import team.da.EntrantDALayer;
import team.da.jpa.converter.EntrantInfoMapper;
import team.da.jpa.entity.EntrantEntity;
import team.da.jpa.repository.EntrantEntityRepository;

@Service
@Profile("SPRING_DATA")
public class JPAEntrantDA implements EntrantDALayer {


    /**
     * Зависимость.
     */
    @Autowired
    private EntrantEntityRepository entrantRepo;


    /**
     * Маппер.
     */
    @Autowired
    private EntrantInfoMapper mapper;

    /**
     * Регистрация пользователя.
     *
     * @param entrantInfo
     * @return
     */
    @Override
    public EntrantInfo register(final EntrantInfo entrantInfo) {
        EntrantEntity entity = mapper.map(entrantInfo);
        entity.setEnabled(true);
        return mapper.map(entrantRepo.save(entity));
    }

    /**
     * Поиск пользователя.
     *
     * @param username
     * @return
     */
    @Override
    public EntrantInfo findByUsername(final String username) {
        return mapper.map(entrantRepo.findByUsername(username));
    }
}
