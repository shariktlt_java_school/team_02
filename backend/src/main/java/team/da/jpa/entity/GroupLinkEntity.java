package team.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Getter
@Setter
@Table(name = "groups_link")
public class GroupLinkEntity {
    /**
     * Встраиваемая сущность составного ключа.
     */
    @EmbeddedId
    private GroupLinkId pk;

    /**
     * Связь многие к одному с таблицей groups через поле group_id.
     */
    @ManyToOne
    @JoinColumn(name = "group_id", insertable = false, updatable = false)
    private GroupEntity group;

    /**
     * Связь многие к одному с таблицей users через поле user_id.
     */
    @ManyToOne
    @JoinColumn(name = "user_id", insertable = false, updatable = false)
    private UserEntity user;


    /**
     * Встраиваемая сущность полей составного ключа таблицы.
     */
    @Embeddable
    @Getter
    @Setter
    public static class GroupLinkId implements Serializable {

        /**
         * Составная часть ключа user_id.
         */
        @Column(name = "user_id")
        private String userId;

        /**
         * Составная часть ключа group_id.
         */
        @Column(name = "group_id")
        private String groupId;
    }

    /**
     * Билдер первичного ключа.
     * @param userId
     * @param groupId
     * @return id
     */
    public static GroupLinkId pk(final String userId, final String groupId) {
        GroupLinkId id = new GroupLinkId();
        id.setUserId(userId);
        id.setGroupId(groupId);
        return id;
    }

}
