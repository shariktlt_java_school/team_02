package team.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Table(name = "users")
public class UserEntity {

    /**
     * Id пользователя.
     */
    @Id
    private String userId;

    /**
     * Имя.
     */
    @Column(name = "first_name")
    private String firstName;

    /**
     * Фамилия.
     */
    @Column(name = "last_name")
    private String lastName;

    /**
     * Дата рождения.
     */
    @Column(name = "birth_date")
    private String birthDate;

    /**
     * Номер телефона.
     */
    @Column(name = "phone")
    private String phone;

    /**
     * Адрес email.
     */
    @Column(name = "email")
    private String email;

    /**
     * Роль.
     */
    @Column(name = "role")
    private String role;

}
