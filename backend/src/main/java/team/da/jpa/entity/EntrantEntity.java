package team.da.jpa.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "entrants")
@Getter
@Setter
public class EntrantEntity {
    /**
     * Id.
     */
    @Id
    private String id;

    /**
     * Username.
     */
    private String username;

    /**
     * Password.
     */
    private String password;

    /**
     * Roles.
     */
    private String roles;

    /**
     * Enabled.
     */
    private Boolean enabled;
}
