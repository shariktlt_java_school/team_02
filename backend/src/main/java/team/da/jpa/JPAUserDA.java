package team.da.jpa;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import team.api.user.User;
import team.api.user.UserForm;
import team.da.UserDALayer;
import team.da.jpa.converter.UserMapper;
import team.da.jpa.entity.GroupEntity;
import team.da.jpa.entity.GroupLinkEntity;
import team.da.jpa.entity.UserEntity;
import team.da.jpa.repository.EntrantEntityRepository;
import team.da.jpa.repository.GroupEntityRepository;
import team.da.jpa.repository.GroupLinkEntityRepository;
import team.da.jpa.repository.UserEntityRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
@Profile("SPRING_DATA")
public class JPAUserDA implements UserDALayer {

    /**
     * Зависимость на UserEntityRepository.
     */
    @Autowired
    private UserEntityRepository userRepo;

    /**
     * Зависимость на link репозиторий.
     */
    @Autowired
    private GroupLinkEntityRepository linkRepo;

    /**
     * Зависимость на Маппер.
     */
    @Autowired
    private UserMapper userMapper;

    /**
     * Зависимость на GroupEntityRepo.
     */
    @Autowired
    private GroupEntityRepository groupRepo;

    /**
     * Зависимость на EntrantRepo.
     */
    @Autowired
    private EntrantEntityRepository entrantRepo;

    /**
     * Связывание пользователя и группы.
     *
     * @param userId
     * @param groupId
     * @return message
     */
    public String linkUser(final String userId, final String groupId) {
        GroupLinkEntity entity = new GroupLinkEntity();
        entity.setPk(GroupLinkEntity.pk(userId, groupId));

        Optional<GroupEntity> groupEntitites = groupRepo.findById(groupId);

        if (!groupEntitites.isPresent()) {
            throw new RuntimeException("Group not found by id");
        }

        entity.setGroup(groupEntitites.get());
        linkRepo.save(entity);

        return "Link added";
    }


    /**
     * Получение пользователя по ID.
     *
     * @param userId
     * @return User
     */
    @Override
    public User getUserById(final String userId) {
        return userMapper.map(getUserEntityById(userId));
    }

    /**
     * Получение UserEntity.
     *
     * @param userId
     * @return UserEntity
     */
    private UserEntity getUserEntityById(final String userId) {
        Optional<UserEntity> userEntity = userRepo.findById(userId);
        if (!userEntity.isPresent()) {
            throw new RuntimeException("Group not found by id");
        }
        return userEntity.get();
    }

    /**
     * Удаление пользователя по ID.
     *
     * @param userId
     * @return String
     */
    @Override
    public String deleteUserById(final String userId) {
        linkRepo.deleteAll(linkRepo.findAllByPkUserId(userId));
        userRepo.delete(getUserEntityById(userId));

        try {
            entrantRepo.deleteById(userId);
        } catch (EmptyResultDataAccessException ex) {
            return "DELETED";
        }
        return "DELETED";
    }

    /**
     * Создание пользователя.
     * по умолчанию - студент.
     *
     * @param form
     * @return User
     */
    @Override
    public User createUser(final UserForm form) {
        User user = User.builder()
                .userId(UUID.randomUUID().toString())
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .birthDate(form.getBirthDate())
                .phone(form.getPhone())
                .email(form.getEmail())
                .role("Student")
                .build();
        userRepo.save(userMapper.map(user));

        return user;
    }

    /**
     * Получение всех пользователей.
     *
     * @return List User
     */
    @Override
    public List<User> getAllUsers() {
        return userMapper.mapList(userRepo.findAll());
    }

    /**
     * Обновление информации о пользователе.
     *
     * @param user
     * @return User
     */
    @Override
    public User updateUser(final User user) {
        userRepo.save(userMapper.map(user));
        return user;
    }

    /**
     * Получение списка пользователей по Id группы.
     *
     * @param groupId
     * @return UsersList
     */
    @Override
    public List<User> getUsersByPkGroupId(final String groupId) {
        List<GroupLinkEntity> userEntities = linkRepo.findAllByPkGroupId(groupId);

        if (userEntities == null) {
            List<User> userList = new ArrayList<>();
            return userList;
        }

        return userMapper.mapList(userEntities.stream()
                .map(GroupLinkEntity::getUser)
                .collect(Collectors.toList()));
    }

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     */
    @Override
    public void putUserInGroup(final String userId, final String groupId) {
        linkUser(userId, groupId);
    }
}
