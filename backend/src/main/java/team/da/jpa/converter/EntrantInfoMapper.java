package team.da.jpa.converter;

import org.mapstruct.Mapper;
import team.api.entrant.EntrantInfo;
import team.da.jpa.entity.EntrantEntity;

@Mapper(componentModel = "spring")
public interface EntrantInfoMapper {

    /**
     * Маппинг Entity -> Object.
     *
     * @param entity
     * @return obj
     */
    EntrantInfo map(EntrantEntity entity);

    /**
     * Маппинг Obj -> Entity.
     *
     * @param obj
     * @return entity
     */
    EntrantEntity map(EntrantInfo obj);
}
