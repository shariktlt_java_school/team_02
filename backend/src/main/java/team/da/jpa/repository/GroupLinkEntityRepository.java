package team.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import team.da.jpa.entity.GroupLinkEntity;

import java.util.List;

@Repository
public interface GroupLinkEntityRepository extends
        CrudRepository<GroupLinkEntity, GroupLinkEntity.GroupLinkId> {

    /**
     * Поиск записей по полю составного ключа pk.UserId.
     *
     * @param userId
     * @return List Entities
     */
    List<GroupLinkEntity> findAllByPkUserId(String userId);

    /**
     * Поиск записей по полю составного ключа pk.GroupId.
     *
     * @param groupId
     * @return List Entities
     */
    List<GroupLinkEntity> findAllByPkGroupId(String groupId);


}
