package team.da.jpa.repository;

import org.springframework.data.repository.CrudRepository;
import team.da.jpa.entity.EntrantEntity;

public interface EntrantEntityRepository extends CrudRepository<EntrantEntity, String> {

    /**
     * Поиск записей.
     *
     * @param username
     * @return entrantEntity
     */
    EntrantEntity findByUsername(String username);
}
