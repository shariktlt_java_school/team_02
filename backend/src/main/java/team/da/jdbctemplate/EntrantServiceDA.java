package team.da.jdbctemplate;

import lombok.SneakyThrows;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import team.api.entrant.EntrantInfo;
import team.da.EntrantDALayer;

import java.sql.ResultSet;
import java.util.HashMap;

@Service
@Profile("JDBC_TEMPLATE")
public class EntrantServiceDA implements EntrantDALayer {

    /**
     * Поле в БД.
     */
    public static final String SELECT_USER = "SELECT * FROM entrants WHERE username = ?";

    /**
     * Поле в БД.
     */
    public static final String ID_ATTR = "id";

    /**
     * Поле в БД.
     */
    public static final String USERNAME_ATTR = "username";

    /**
     * Поле в БД.
     */
    public static final String PASSWORD_ATTR = "password";

    /**
     * Поле в БД.
     */
    public static final String ROLES_ATTR = "roles";

    /**
     * Поле в БД.
     */
    public static final String ENABLED_ATTR = "enabled";

    /**
     * Шаблон для вставки.
     */
    private final SimpleJdbcInsert simpleInsert;


    /**
     * Шаблон jdbc.
     */
    private final JdbcTemplate template;


    /**
     * Конструктор.
     *
     * @param jdbcInsertBean
     * @param jdbcTemplateBean
     */
    public EntrantServiceDA(final SimpleJdbcInsert jdbcInsertBean, final JdbcTemplate jdbcTemplateBean) {
        simpleInsert = jdbcInsertBean.withTableName("entrants").usingGeneratedKeyColumns(ID_ATTR);
        template = jdbcTemplateBean;
    }


    /**
     * Регистрация пользователя.
     *
     * @param entrantInfo
     * @return
     */
    @Override
    public EntrantInfo register(final EntrantInfo entrantInfo) {
        HashMap<String, Object> args = new HashMap<>();
        args.put(USERNAME_ATTR, entrantInfo.getUsername());
        args.put(PASSWORD_ATTR, entrantInfo.getPassword());
        args.put(ROLES_ATTR, entrantInfo.getRoles());
        args.put(ENABLED_ATTR, true);
        String id = simpleInsert.executeAndReturnKey(args).toString();

        return EntrantInfo.builder()
                .id(id)
                .username(entrantInfo.getUsername())
                .password(entrantInfo.getPassword())
                .roles(entrantInfo.getRoles())
                .enabled(true)
                .build();
    }

    /**
     * Поиск пользователя.
     *
     * @param username
     * @return entrantInfo
     */
    @Override
    public EntrantInfo findByUsername(final String username) {
        return template.query(SELECT_USER, this::map, username);
    }

    /**
     * Маппер запроса.
     *
     * @param resultSet
     * @return entrantInfo
     */
    @SneakyThrows
    private EntrantInfo map(final ResultSet resultSet) {
        if (!resultSet.next()) {
            return null;
        }
        return EntrantInfo.builder()
                .id(resultSet.getString(ID_ATTR))
                .username(resultSet.getString(USERNAME_ATTR))
                .password(resultSet.getString(PASSWORD_ATTR))
                .roles(resultSet.getString(ROLES_ATTR))
                .build();
    }

}
