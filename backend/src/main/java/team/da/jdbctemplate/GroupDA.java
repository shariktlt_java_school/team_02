package team.da.jdbctemplate;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Component;
import team.api.group.Group;
import team.api.group.GroupForm;
import team.da.GroupDALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Component
@Profile("JDBC_TEMPLATE")
public class GroupDA implements GroupDALayer {
    /**
     * Запрос группы по ID.
     */
    public static final String SELECT_BY_ID = "SELECT * FROM \"groups\" WHERE group_id = ?";

    /**
     * Запрос всех групп.
     */
    public static final String SELECT_ALL_GROUPS = "SELECT * FROM \"groups\"";

    /**
     * Запрос для связки Группы и Пользователя.
     */
    public static final String QUERY_FOR_LINK = "INSERT INTO groups_link (user_id, group_id) VALUES(?, ?)";

    /**
     * Запрос на получение списка групп.
     */
    public static final String SELECT_FROM_GROUPS_BY_USER_ID = "SELECT * FROM groups_link"
            + " WHERE user_id = ?";

    /**
     * Запрос на удаление из таблицы.
     */
    public static final String DELETE_QUERY = "DELETE from \"groups\" WHERE group_id = ?";

    /**
     * Запрос на обновление группы.
     */
    public static final String QUERY_UPDATE_GROUP = "UPDATE \"groups\" SET "
            + "group_name = :group_name, description = :description "
            + "WHERE group_id = :group_id";


    /**
     * Запрос на удаление связи.
     */
    public static final String DELETE_FROM_GROUPS_LINK = "DELETE from groups_link WHERE group_id = ?";


    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;


    /**
     * Зависимость на шаблон jdbc insert.
     */
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("\"groups\"")
                .usingColumns("group_name", "description", "group_id");

    }

    /**
     * Связывание пользователя и группы.
     *
     * @param userId
     * @param groupId
     */
    @Override
    public String linkUser(final String userId, final String groupId) {
        jdbcTemplate.update(QUERY_FOR_LINK, userId, groupId);
        return "link added";
    }

    /**
     * Получение всех доступных групп.
     *
     * @return List
     */
    @Override
    public List<Group> getAllGroups() {
        return jdbcTemplate.query(SELECT_ALL_GROUPS, this::rowMapper);
    }

    /**
     * Получение группы по Id.
     *
     * @param groupId
     * @return Group
     */
    @Override
    public Group getGroupById(final String groupId) {
        return jdbcTemplate.query(SELECT_BY_ID, this::singleRowMapper, groupId);
    }

    /**
     * Создание группы.
     *
     * @param groupForm
     * @return Group
     */
    @Override
    public Group createGroup(final GroupForm groupForm) {
        Group group = Group.builder()
                .groupId(UUID.randomUUID().toString())
                .groupName(groupForm.getGroupName())
                .description(groupForm.getDescription())
                .build();

        jdbcInsert.execute(toMap(group));

        return group;
    }

    /**
     * Удаление группы по Id.
     *
     * @param groupId
     * @return Group
     */
    @Override
    public String deleteGroupById(final String groupId) {
        jdbcTemplate.update(DELETE_QUERY, groupId);
        jdbcTemplate.update(DELETE_FROM_GROUPS_LINK, groupId);
        return "DELETED";

    }


    /**
     * Обновление информации о группе.
     *
     * @param group
     * @return Group
     */
    @Override
    public Group updateGroupById(final Group group) {
        jdbcNamed.update(QUERY_UPDATE_GROUP, toMap(group));
        return group;

    }

    /**
     * получить список групп в которых состоит пользователь.
     *
     * @param userId
     * @return GroupList
     */
    @Override
    public List<Group> getGroupsByPkUserId(final String userId) {
        List<String> list = jdbcTemplate.query(SELECT_FROM_GROUPS_BY_USER_ID,
                this::uuidMap, userId);

        return list.stream()
                .map(this::getGroupById)
                .collect(Collectors.toList());
    }

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     */
    @Override
    public void putUserInGroup(final String userId, final String groupId) {
        linkUser(userId, groupId);

    }

    /**
     * Map для передачи в запрос.
     *
     * @param group
     * @return map
     */
    private Map<String, Object> toMap(final Group group) {
        Map<String, Object> map = new HashMap<>();

        map.put("group_id", group.getGroupId());
        map.put("group_name", group.getGroupName());
        map.put("description", group.getDescription());
        return map;
    }


    @SneakyThrows
    private Group rowMapper(final ResultSet rs, final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private Group singleRowMapper(final ResultSet rs) {
        rs.next();
        return mapRow(rs);
    }

    private Group mapRow(final ResultSet resultSet) throws SQLException {
        return Group.builder()
                .groupId(resultSet.getString("group_id"))
                .groupName(resultSet.getString("group_name"))
                .description(resultSet.getString("description"))
                .build();
    }

    private String uuidMap(final ResultSet resultSet, final int pos) throws SQLException {
        return resultSet.getString("group_id");
    }
}
