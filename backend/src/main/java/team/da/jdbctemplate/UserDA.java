package team.da.jdbctemplate;

import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Service;
import team.api.user.User;
import team.api.user.UserForm;
import team.da.UserDALayer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

/**
 * Data Access слой для users.
 */
@Service
@Profile("JDBC_TEMPLATE")
public class UserDA implements UserDALayer {

    /**
     * Обновление информации о пользователе.
     */
    public static final String QUERY_UPDATE_USER = "UPDATE users SET "
            + "first_name = :first_name, last_name = :last_name, "
            + "birth_date = :birth_date, phone = :phone, "
            + "email = :email, role = :role "
            + "WHERE user_id = :user_id";


    /**
     * Запрос на получение списка рользователей в группе.
     */
    public static final String SELECT_FROM_USERS_BY_GROUP_ID = "SELECT * FROM groups_link"
            + " WHERE group_id = ?";

    /**
     * Запрос на поиск по ID.
     */
    public static final String QUERY_SELECT_BY_ID = "SELECT * from users WHERE user_id = ?";

    /**
     * Запрос на удаление из таблицы.
     */
    public static final String DELETE_QUERY = "DELETE from users WHERE user_id = ?";

    /**
     * Запрос всех пользователей.
     */
    public static final String SELECT_ALL_USERS = "SELECT * FROM users";

    /**
     * Запрос для связки Группы и Пользователя.
     */
    public static final String QUERY_FOR_LINK = "INSERT INTO groups_link (user_id, group_id) VALUES(?, ?)";


    /**
     * Запрос на удаление связи.
     */
    public static final String DELETE_FROM_GROUPS_LINK = "DELETE from groups_link WHERE user_id = ?";

    /**
     * Запрос на удаление Entrant.
     */
    public static final String DELETE_FROM_ENTRANTS = "DELETE from entrants WHERE id = ?";

    /**
     * Зависимость на шаблон jdbc.
     */
    @Autowired
    private JdbcTemplate jdbcTemplate;


    /**
     * Зависимость на шаблон jdbc insert.
     */
    private SimpleJdbcInsert jdbcInsert;

    /**
     * Зависимость на шаблон jdbc named.
     */
    @Autowired
    private NamedParameterJdbcTemplate jdbcNamed;

    /**
     * Внедрение зависимости jdbc insert с настройкой для таблицы.
     *
     * @param bean
     */
    @Autowired
    public void setJdbcInsert(final SimpleJdbcInsert bean) {
        jdbcInsert = bean
                .withTableName("users");
    }


    /**
     * Получение пользователя по ID.
     *
     * @param userId
     * @return User
     */
    @Override
    public User getUserById(final String userId) {
        return jdbcTemplate.query(QUERY_SELECT_BY_ID,
                this::singleRowMapper, userId);
    }

    /**
     * Удаление пользователя по ID.
     *
     * @param userId
     * @return String
     */
    @Override
    public String deleteUserById(final String userId) {
        jdbcTemplate.update(DELETE_QUERY, userId);
        jdbcTemplate.update(DELETE_FROM_GROUPS_LINK, userId);
        jdbcTemplate.update(DELETE_FROM_ENTRANTS, userId);
        return "DELETED";
    }

    /**
     * Создание пользователя.
     * по умолчанию - студент.
     *
     * @param form
     * @return User
     */
    @Override
    public User createUser(final UserForm form) {
        User user = User.builder()
                .userId(UUID.randomUUID().toString())
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .birthDate(form.getBirthDate())
                .phone(form.getPhone())
                .email(form.getEmail())
                .role("Student")
                .build();

        jdbcInsert.execute(toMap(user));
        return user;
    }

    /**
     * Получение всех пользователей.
     *
     * @return List User
     */
    @Override
    public List<User> getAllUsers() {
        return jdbcTemplate.query(SELECT_ALL_USERS, this::rowMapper);
    }

    /**
     * Обновление информации о пользователе.
     *
     * @param user
     * @return User
     */
    @Override
    public User updateUser(final User user) {
        jdbcNamed.update(QUERY_UPDATE_USER, toMap(user));
        return user;
    }

    /**
     * Map для передачи в запрос.
     *
     * @param user
     * @return map
     */
    private Map<String, Object> toMap(final User user) {
        Map<String, Object> map = new HashMap<>();

        map.put("user_id", user.getUserId());
        map.put("first_name", user.getFirstName());
        map.put("last_name", user.getLastName());
        map.put("birth_date", user.getBirthDate());
        map.put("phone", user.getPhone());
        map.put("email", user.getEmail());
        map.put("role", user.getRole());

        return map;
    }

    @SneakyThrows
    private User rowMapper(final ResultSet rs, final int pos) {
        return mapRow(rs);
    }

    @SneakyThrows
    private User singleRowMapper(final ResultSet rs) {
        rs.next();
        return mapRow(rs);
    }

    private User mapRow(final ResultSet rs) throws SQLException {
        return User.builder()
                .userId(rs.getString("user_id"))
                .firstName(rs.getString("first_name"))
                .lastName(rs.getString("last_name"))
                .birthDate(rs.getString("birth_date"))
                .phone(rs.getString("phone"))
                .email(rs.getString("email"))
                .role(rs.getString("role"))
                .build();

    }

    /**
     * Получение списка пользователей по Id группы.
     *
     * @param groupId
     * @return UsersList
     */
    @Override
    public List<User> getUsersByPkGroupId(final String groupId) {
        List<String> userIds = jdbcTemplate.query(SELECT_FROM_USERS_BY_GROUP_ID, this::uuidMap, groupId);

        return userIds.stream()
                .map(this::getUserById)
                .collect(Collectors.toList());
    }

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     */
    @Override
    public void putUserInGroup(final String userId, final String groupId) {
        linkUser(userId, groupId);

    }

    /**
     * Связывание пользователя и группы.
     *
     * @param userId
     * @param groupId
     */
    public void linkUser(final String userId, final String groupId) {
        jdbcTemplate.update(QUERY_FOR_LINK, userId, groupId);
    }

    private String uuidMap(final ResultSet resultSet, final int pos) throws SQLException {
        return resultSet.getString("user_id");
    }
}
