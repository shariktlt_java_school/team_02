package team.da;

import team.api.user.User;
import team.api.user.UserForm;

import java.util.List;

public interface UserDALayer {

    /**
     * Получение пользователя по ID.
     *
     * @param userId
     * @return User
     */
    User getUserById(String userId);

    /**
     * Удаление пользователя по ID.
     *
     * @param userId
     * @return String
     */
    String deleteUserById(String userId);

    /**
     * Создание пользователя.
     * по умолчанию - студент.
     *
     * @param form
     * @return User
     */
    User createUser(UserForm form);

    /**
     * Получение всех пользователей.
     *
     * @return List User
     */
    List<User> getAllUsers();

    /**
     * Обновление информации о пользователе.
     *
     * @param user
     * @return User
     */
    User updateUser(User user);

    /**
     * Получение списка пользователей по Id группы.
     *
     * @param groupId
     * @return UsersList
     */
    List<User> getUsersByPkGroupId(String groupId);

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     */
    void putUserInGroup(String userId, String groupId);

}
