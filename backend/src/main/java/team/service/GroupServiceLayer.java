package team.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import team.api.group.Group;
import team.api.group.GroupForm;
import team.api.group.GroupService;
import team.da.GroupDALayer;

import java.util.List;

@Service
@Profile("!STUBS")
@Qualifier("GroupServiceLayer")
public class GroupServiceLayer implements GroupService {

    /**
     * Зависимость для слоя додступа к данным групп.
     */
    @Autowired
    private GroupDALayer daLayer;


    /**
     * Получение всех доступных групп.
     *
     * @return List
     */
    @Override
    public List<Group> getAllGroups() {
        return daLayer.getAllGroups();
    }

    /**
     * Получение группы по Id.
     *
     * @param groupId
     * @return Group
     */
    @Override
    public Group getGroupById(final String groupId) {
        return daLayer.getGroupById(groupId);
    }

    /**
     * Создание группы.
     *
     * @param groupForm
     * @return Group
     */
    @Override
    public Group createGroup(final GroupForm groupForm) {
        return daLayer.createGroup(groupForm);
    }

    /**
     * Удаление группы по Id.
     *
     * @param groupId
     * @return Group
     */
    @Override
    public String deleteGroupById(final String groupId) {
        daLayer.deleteGroupById(groupId);
        return "DELETED";
    }

    /**
     * Обновление информации о группе.
     *
     * @param group
     * @return Group
     */
    @Override
    public Group updateGroup(final Group group) {
        return daLayer.updateGroupById(group);
    }

    /**
     * получить список групп в которых состоит пользователь.
     *
     * @param userId
     * @return GroupList
     */
    @Override
    public List<Group> getGroupByUserId(final String userId) {
        return daLayer.getGroupsByPkUserId(userId);
    }

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     * @return message
     */
    @Override
    public String putUserInGroup(final String userId, final String groupId) {
        daLayer.putUserInGroup(userId, groupId);
        return "DONE";
    }

    /**
     * Связывание заявки и услуги.
     *
     * @param userId
     * @param ids
     * @return message
     */
    public String link(final String userId, final List<String> ids) {
        ids.forEach(id -> daLayer.linkUser(userId, id));
        return "linking done";
    }
}
