package team.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;
import team.api.user.User;
import team.api.user.UserForm;
import team.api.user.UserService;
import team.da.UserDALayer;

import java.util.List;

@Service
@Profile("!STUBS")
@Qualifier("UserServiceLayer")
public class UserServiceLayer implements UserService {

    /**
     * Зависимость для слоя доступа к данным пользователя.
     */
    @Autowired
    private UserDALayer daLayer;


    /**
     * Зависимость для сервиса групп.
     */
    @Autowired
    private GroupServiceLayer groupService;


    /**
     * Получение пользователя по ID.
     *
     * @param userId
     * @return User
     */
    @Override
    public User getUserById(final String userId) {
        return daLayer.getUserById(userId);
    }

    /**
     * Удаление пользователя по ID.
     *
     * @param userId
     * @return User
     */
    @Override
    public String deleteUserById(final String userId) {
        daLayer.deleteUserById(userId);
        return "DELETED";
    }

    /**
     * Создание пользователя.
     * по умолчанию - студент.
     *
     * @param form
     * @return User
     */
    @Override
    public User createUser(final UserForm form) {
        return daLayer.createUser(form);
    }

    /**
     * Получение всех пользователей.
     *
     * @return List User
     */
    @Override
    public List<User> getAllUsers() {
        return daLayer.getAllUsers();
    }

    /**
     * Обновление информации о пользователе.
     *
     * @param user
     * @return User
     */
    @Override
    public User updateUser(final User user) {
        return daLayer.updateUser(user);
    }

    /**
     * Получение списка пользователей по Id группы.
     *
     * @param groupId
     * @return UsersList
     */
    @Override
    public List<User> getUsersByGroupId(final String groupId) {
        return daLayer.getUsersByPkGroupId(groupId);
    }

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     */
    @Override
    public void putUserInGroup(final String userId, final String groupId) {
        daLayer.putUserInGroup(userId, groupId);
    }
}
