package team.authorization;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.parameters.P;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import team.api.entrant.EntrantInfo;
import team.api.entrant.EntrantService;
import team.api.user.User;
import team.api.user.UserForm;
import team.api.user.UserService;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class FrontendEntrantServiceTest {

    public static final String USERNAME = "username";
    public static final String ID = "Id";
    public static final String PASSWORD = "password";
    public static final String ROLE = "role";

    @Mock
    private EntrantService entrantService;

    @Mock
    private UserService userService;

    @Mock
    User userMock;

    @Mock
    EntrantInfo entrantInfo;

    @Mock
    private PasswordEncoder passwordEncoder;

    @InjectMocks
    private FrontendEntrantService frontendEntrantService;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        when(entrantService.loadUserByUsername(USERNAME)).thenReturn(entrantInfo);
        when(entrantInfo.getId()).thenReturn(ID);
        when(entrantInfo.getUsername()).thenReturn(USERNAME);
        when(entrantInfo.getPassword()).thenReturn(PASSWORD);
        when(entrantInfo.getRoles()).thenReturn(ROLE);

        when(userService.createUser(any(UserForm.class))).thenReturn(userMock);
        when(userMock.getUserId()).thenReturn(ID);

        when(passwordEncoder.encode(PASSWORD)).thenReturn(PASSWORD);
        when(entrantService.register(any(EntrantInfo.class))).thenReturn(ID);
    }

    @Test
    public void insertRow() {
        String expected = frontendEntrantService.insertRow(USERNAME, PASSWORD, ROLE);
        assertEquals(ID, expected);
    }

    @Test
    public void loadUserByUsername() {

        UserDetails userDetails = frontendEntrantService.loadUserByUsername(USERNAME);

        assertEquals(userDetails.getUsername(), USERNAME);
        assertEquals(userDetails.getPassword(), PASSWORD);
    }

}