package team.authorization;


import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class EntrantDetailsIdTest {


    @Test
    public void start() {
        String id = "id";
        String userName = "name";
        String password = "pass";
        String role = "role";
        EntrantDetailsId detailsId = new EntrantDetailsId(id, userName, password, role);
        assertEquals(detailsId.getUserId(), "id");
        assertEquals(detailsId.getUsername(), "name");
        assertEquals(detailsId.getPassword(), "pass");
        assertNotNull(detailsId.getAuthorities());
    }
}