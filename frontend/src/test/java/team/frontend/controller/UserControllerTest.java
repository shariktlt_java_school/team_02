package team.frontend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import team.api.group.Group;
import team.api.group.GroupService;
import team.api.user.User;
import team.api.user.UserForm;
import team.api.user.UserService;
import team.authorization.EntrantDetailsId;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;


public class UserControllerTest {

    private static final String USER_ID = "123";

    @Mock
    private User expectedUser;

    @Mock
    private Model modelMock;

    @Mock
    private UserService userService;

    @Mock
    private GroupService groupService;

    @Mock
    UserForm userForm;

    @Mock
    private Authentication authentication;

    @Mock
    private Authentication authExcept;

    @Mock
    private EntrantDetailsId entrantDetailsId;

    @InjectMocks
    private UserController userController;

    private List<Group> groupList = new ArrayList<>();
    private List<Group> groupList2 = new ArrayList<>();
    private List<User> userList = new ArrayList<>();

    @Before
    public void setUp() throws Exception {
        openMocks(this);
        when(authExcept.getPrincipal()).thenReturn(new Object());
        when(authentication.getPrincipal()).thenReturn(entrantDetailsId);
        when(entrantDetailsId.getUserId()).thenReturn(USER_ID);

        groupList.add(mock(Group.class));
        groupList.add(mock(Group.class));
        groupList.add(mock(Group.class));

        groupList2.add(mock(Group.class));
        groupList2.add(mock(Group.class));

        userList.add(mock(User.class));
        userList.add(mock(User.class));
        userList.add(mock(User.class));

        when(userForm.getFirstName()).thenReturn("firstName");
        when(userForm.getLastName()).thenReturn("lastName");
        when(userForm.getBirthDate()).thenReturn("birthDate");
        when(userForm.getPhone()).thenReturn("phone");
        when(userForm.getEmail()).thenReturn("email");
        when(userForm.getRole()).thenReturn("role");

        when(expectedUser.getFirstName()).thenReturn("firstName1");
        when(expectedUser.getLastName()).thenReturn("lastName1");
        when(expectedUser.getBirthDate()).thenReturn("birthDate1");
        when(expectedUser.getPhone()).thenReturn("phone1");
        when(expectedUser.getEmail()).thenReturn("email1");
        when(expectedUser.getRole()).thenReturn("role1");
    }

    @Test
    public void index() {
        when(userService.getUserById(USER_ID)).thenReturn(expectedUser);
        ModelAndView view = userController.index(authentication);
        assertEquals(view.getModel().get("user"), expectedUser);
        verify(userService, times(1)).getUserById(USER_ID);
    }

    @Test
    public void testIndex() {
        when(userService.getAllUsers()).thenReturn(userList);
        String expect = userController.index(modelMock, authentication);

        assertEquals("user/viewall", expect);
        verify(modelMock).addAttribute("users", userList);
        verify(modelMock, times(2)).addAttribute(anyString(), any());

    }

    @Test(expected = IllegalStateException.class)
    public void getUserId() {
        String expect = userController.index(modelMock, authExcept);

    }

    @Test
    public void view() {

        when(userService.getUserById(USER_ID)).thenReturn(expectedUser);
        when(groupService.getAllGroups()).thenReturn(groupList);
        when(groupService.getGroupByUserId(USER_ID)).thenReturn(groupList2);

        ModelAndView view = userController.view("123");
        assertEquals("user/viewuserinfo", view.getViewName());

        assertEquals(expectedUser, view.getModel().get("user"));
        assertEquals(groupList, view.getModel().get("groups"));
        assertEquals(groupList2, view.getModel().get("usergroup"));
    }

    @Test
    public void viewNull() {
        User user = null;
        when(userService.getUserById(USER_ID)).thenReturn(user);

        ModelAndView view = userController.view("123");
        assertEquals("user/usernotfound", view.getViewName());

    }

    @Test
    public void delete() {
        String redirect = userController.delete(USER_ID);
        assertEquals("redirect:/user/viewall", redirect);
    }

    @Test
    public void createForm() {
        when(modelMock.addAttribute("userForm", UserForm.class)).thenAnswer(i -> {
            UserForm userForm = i.getArgument(0, UserForm.class);
            return null;
        });

        String expected = userController.createForm(modelMock);

        assertEquals(any(UserForm.class), modelMock.getAttribute("userForm"));
        assertEquals("user/create", expected);
    }

    @Test
    public void createUser() {

    User expectedUser = User.builder().userId("id").build();

    when(userService.createUser(any(UserForm.class))).thenAnswer(invocation -> {
        UserForm form = invocation.getArgument(0, UserForm.class);
        assertEquals(form.getFirstName(), userForm.getFirstName());
        assertEquals(form.getLastName(), userForm.getLastName());
        assertEquals(form.getBirthDate(), userForm.getBirthDate());
        assertEquals(form.getPhone(), userForm.getPhone());
        assertEquals(form.getEmail(), userForm.getEmail());
        assertEquals(form.getRole(), userForm.getRole());
        return expectedUser;
    });

    String expect = userController.createUser(userForm, modelMock);
    verify(userService).createUser(any(UserForm.class));
    assertEquals("redirect:/user/viewall", expect);

    }

    @Test
    public void updateForm() {
        when(userService.getUserById(USER_ID)).thenReturn(expectedUser);
        String expect = userController.updateForm(USER_ID, modelMock);
        verify(modelMock).addAttribute("user", expectedUser);

        assertEquals("user/update", expect);
    }

    @Test
    public void updateUser() {

    }

    @Test
    public void putUserInGroup() {
        when(userService.getUserById(USER_ID)).thenReturn(expectedUser);
        when(expectedUser.getUserId()).thenReturn(USER_ID);

        assertEquals("redirect:/user/viewuserinfo/" + expectedUser.getUserId(), userController.putUserInGroup(USER_ID, "GROUP"));
    }
}