package team.frontend.controller;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;
import team.api.group.Group;
import team.api.group.GroupForm;
import team.api.group.GroupService;
import team.api.user.User;
import team.api.user.UserService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.MockitoAnnotations.openMocks;

public class GroupControllerTest {

    public static final String GROUP_ID = "groupID";
    public static final String USER_ID = "userId";
    public static final String GROUP = "group";
    public static final String GROUPS = "groups";

    @Mock
    private Model modelMock;

    @Mock
    private UserService userService;

    @Mock
    private GroupService groupService;

    @Mock
    private Authentication authentication;

    @Mock
    private GroupForm groupForm;

    @Mock
    Group expectedGroup;

    private List<Group> groupList = new ArrayList<>();

    private List<User> userList = new ArrayList<>();

    @InjectMocks
    private GroupController groupController;


    @Before
    public void setUp() throws Exception {
        openMocks(this);

        groupList.add(mock(Group.class));
        groupList.add(mock(Group.class));

        userList.add(mock(User.class));
        userList.add(mock(User.class));
        userList.add(mock(User.class));

        when(groupForm.getGroupName()).thenReturn("groupName");
        when(groupForm.getDescription()).thenReturn("description");
    }


    @Test
    public void index() {
        String exception = groupController.index();
        assertEquals("group/index", exception);
    }

    @Test
    public void testIndex() {
        when(groupService.getAllGroups()).thenReturn(groupList);
        String expect = groupController.index(modelMock);
        assertEquals("group/viewallgroups", expect);
        verify(modelMock).addAttribute(GROUPS, groupList);

    }

    @Test
    public void view() {
        when(groupService.getGroupById(GROUP_ID)).thenReturn(expectedGroup);
        when(userService.getUsersByGroupId(GROUP_ID)).thenReturn(userList);

        ModelAndView view = groupController.view(GROUP_ID);

        assertEquals(expectedGroup, view.getModel().get(GROUP));
        assertEquals(userList, view.getModel().get("users"));

    }

    @Test
    public void createForm() {
        when(modelMock.addAttribute("groupform", GroupForm.class)).thenAnswer(i -> {
            GroupForm form = i.getArgument(0, GroupForm.class);
            return null;
        });

        String expected = groupController.createForm(modelMock);
        assertEquals(any(GroupForm.class), modelMock.getAttribute("groupform"));
        assertEquals("group/creategroup", expected);
    }

    @Test
    public void createGroup() {
        Group expectedGroup = Group.builder().groupId(GROUP_ID).build();

        when(groupService.createGroup(any(GroupForm.class))).thenAnswer(i -> {
            GroupForm form = i.getArgument(0, GroupForm.class);
            assertEquals(groupForm.getGroupName(), form.getGroupName());
            assertEquals(groupForm.getDescription(), form.getDescription());
            return expectedGroup;
        });

        String expected = groupController.createGroup(groupForm, modelMock);
        verify(groupService).createGroup(any(GroupForm.class));
        assertEquals("redirect:/group/viewgroupinfo/" + expectedGroup.getGroupId(), expected);

    }

    @Test
    public void updateForm() {
        when(groupService.getGroupById(GROUP_ID)).thenReturn(expectedGroup);
        String expect = groupController.updateForm(GROUP_ID, modelMock);
        verify(modelMock).addAttribute(GROUP, expectedGroup);
        assertEquals("group/updategroup", expect);
    }

    @Test
    public void updateGroup() {
Group expectGroup = Group.builder().groupId(GROUP_ID).build();
        when(groupService.updateGroup(any(Group.class))).thenAnswer(i -> {
            Group group = i.getArgument(0, Group.class);
            assertEquals(group.getGroupId(), GROUP_ID);
            assertEquals(group.getGroupName(), groupForm.getGroupName());
            assertEquals(group.getDescription(), groupForm.getDescription());
            return expectGroup;
        });

        String expected = groupController.updateGroup(groupForm, GROUP_ID, modelMock);
        verify(groupService).updateGroup(any(Group.class));
        assertEquals("redirect:/group/viewgroupinfo/"+expectGroup.getGroupId(), expected);
    }

    @Test
    public void delete() {
        String expected = groupController.delete(GROUP_ID);
        assertEquals("redirect:/group/viewallgroups", expected);
    }
}