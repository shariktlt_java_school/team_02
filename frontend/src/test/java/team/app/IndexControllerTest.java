package team.app;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import team.api.entrant.EntrantService;
import team.authorization.FrontendEntrantService;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.openMocks;

public class IndexControllerTest {

    @Mock
    FrontendEntrantService frontendEntrantService;

    @Mock
    private Authentication authentication;

    public static final int MIN_PASS_LENGTH = 4;

    public static final String ROLE_STUDENT_STR = "ROLE_STUDENT";

    public static final String ROLE_TEACHER_STR = "ROLE_TEACHER";

    public static final SimpleGrantedAuthority ROLE_STUDENT = new SimpleGrantedAuthority(ROLE_STUDENT_STR);

    public static final SimpleGrantedAuthority ROLE_TEACHER = new SimpleGrantedAuthority(ROLE_TEACHER_STR);

    List<SimpleGrantedAuthority> auth = new ArrayList<>();


    @InjectMocks
    private IndexController indexController;

    @Before
    public void setUp() throws Exception {
        openMocks(this);
    }


    @Test
    public void index() {
        String expected = indexController.index(null);
        assertEquals("index", expected);

        when(authentication.isAuthenticated()).thenReturn(false);
        expected = indexController.index(authentication);
        assertEquals("index", expected);

        when(authentication.isAuthenticated()).thenReturn(true);
        when(authentication.getAuthorities())
                .thenAnswer(invocationOnMock -> auth);

        auth.add(ROLE_STUDENT);
        expected = indexController.index(authentication);
        assertEquals("redirect:/user/", expected);

        auth.remove(0);
        auth.add(ROLE_TEACHER);

        expected = indexController.index(authentication);
        assertEquals("redirect:/user/", expected);

    }

    @Test
    public void register() {
        String expected = indexController.register();
        assertEquals("register", expected);
    }

    @Test
    public void registerProcess() {
        String username = "name";
        String password1 = "12345";
        String password2 = "12345";
        String password3 = "54321";
        String role;
        String expected;

        role = "ROLE_STUDENT";
        expected = indexController.registerProcess(username, password1, password2, role);
        assertEquals("redirect:/login", expected);

        role = "ROLE_TEACHER";
        expected = indexController.registerProcess(username, password1, password2, role);
        assertEquals("redirect:/login", expected);

        expected = indexController.registerProcess(username, password1, password3, role);
        assertEquals("redirect:/register?bad_password", expected);

//        role = "ROLE_TEACHER";
//        expected = indexController.registerProcess(username, password1, password2, role);
//        verify(frontendEntrantService).insertRow(username, password1, role);
//        assertEquals("redirect:/login", expected);
    }
}