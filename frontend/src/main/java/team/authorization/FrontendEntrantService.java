package team.authorization;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import team.api.entrant.EntrantInfo;
import team.api.entrant.EntrantService;
import team.api.user.User;
import team.api.user.UserForm;
import team.api.user.UserService;

@Service
public class FrontendEntrantService implements UserDetailsService {

    /**
     * Зависимость.
     */
    @Autowired
    private PasswordEncoder passwordEncoder;

    /**
     * Зависимость на бэкенд сервис.
     */
    @Autowired
    private EntrantService entrantService;

    /**
     * Зависимость на сервис пользователей.
     */
    @Autowired
    private UserService userService;

    /**
     * Регистрация пользователя.
     *
     * @param username
     * @param password
     * @param role
     * @return id
     */
    public String insertRow(final String username, final String password, final String role) {
        UserForm userForm = new UserForm();
        userForm.setFirstName(username);
        userForm.setRole(role);
        User user = userService.createUser(userForm);
        String id = user.getUserId();

        return entrantService.register(EntrantInfo.builder()
                .id(id)
                .username(username)
                .password(passwordEncoder.encode(password))
                .roles(role)
                .enabled(true)
                .build());
    }

    /**
     * Поиск информации о пользователе.
     *
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {

        EntrantInfo entrantInfo = entrantService.loadUserByUsername(username);

        if (entrantInfo.getId().equals("N")) {
            throw new UsernameNotFoundException("Entrant not found");
        }

        return new EntrantDetailsId(
                entrantInfo.getId(),
                entrantInfo.getUsername(),
                entrantInfo.getPassword(),
                entrantInfo.getRoles()
        );
    }

}
