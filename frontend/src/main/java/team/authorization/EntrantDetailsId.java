package team.authorization;

import lombok.Getter;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

@Getter
public class EntrantDetailsId extends User {

    /**
     * User id.
     */
    private String userId;

    /**
     * Конструктор.
     *  @param id
     * @param username
     * @param password
     * @param role
     */
    public EntrantDetailsId(final String id, final String username, final String password, final String role) {
        super(username, password, AuthorityUtils.createAuthorityList(role));
        userId = id;
    }

}
