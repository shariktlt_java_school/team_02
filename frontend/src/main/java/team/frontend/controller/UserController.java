package team.frontend.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import team.api.group.Group;
import org.springframework.web.bind.annotation.ModelAttribute;
import team.api.user.UserForm;
import org.springframework.web.bind.annotation.PathVariable;
import team.api.group.GroupService;
import org.springframework.web.bind.annotation.PostMapping;
import team.api.user.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import team.api.user.UserService;
import team.authorization.EntrantDetailsId;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RequestMapping("/user")
@Controller
public class UserController {


    /**
     * Атрибут модели для хранения списка пользователей.
     */
    public static final String USERS_ATTR = "users";

    /**
     * Атрибут для модели пользователя.
     */
    public static final String USER = "user";

    /**
     * Атрибут списка групп.
     */
    public static final String GROUPS_LIST = "groups";

    /**
     * Ссылка на сервис пользователя.
     */
    @Autowired
    private UserService userService;

    /**
     * Ссылка на сервис групп.
     */
    @Autowired
    private GroupService groupService;


    /**
     * Index.
     *
     * @param auth
     * @return userindex
     */
    @GetMapping("/")

    public ModelAndView index(final Authentication auth) {
        ModelAndView model = new ModelAndView("user/index");
        String id = getUserId(auth);
        User logged = userService.getUserById(id);
        model.addObject(USER, logged);
        return model;
    }


    /**
     * Отображение списка пользователей.
     *
     * @param model модель для представления
     * @param auth
     * @return view
     */
    @GetMapping("/viewall")
    public String index(final Model model, final Authentication auth) {
        String id = getUserId(auth);
        User logged = userService.getUserById(id);
        List<User> list = userService.getAllUsers();
        model.addAttribute(USERS_ATTR, list);
        model.addAttribute("logged", logged);
        return "user/viewall";
    }

    /**
     * Подробная информация о пользователе.
     *
     * @param userId
     * @return model
     */
    @GetMapping("/viewuserinfo/{id}")
    public ModelAndView view(@PathVariable("id") final String userId) {
        ModelAndView model = new ModelAndView("user/viewuserinfo");
        User user = userService.getUserById(userId);
        if (user == null) {
            return new ModelAndView("user/usernotfound");
        }
        List<Group> list = groupService.getAllGroups();

        List<Group> userGroup = groupService.getGroupByUserId(userId);
        if (userGroup == null) {
            userGroup = new ArrayList<>();
        }
        model.addObject(GROUPS_LIST, list);
        model.addObject("usergroup", userGroup);
        model.addObject("user", user);

        return model;
    }

    /**
     * Получаем clientId.
     *
     * @param auth
     * @return clientId
     */
    private String getUserId(final Authentication auth) {
        Object principal = auth.getPrincipal();

        if (principal instanceof EntrantDetailsId) {
            return ((EntrantDetailsId) principal).getUserId();
        }
        throw new IllegalStateException("invalid auth.getPrincipal() object type");
    }


    /**
     * Удаление пользователя.
     *
     * @param userId
     * @return redirect
     */
    @GetMapping("/deleteuser/{id}")
    public String delete(@PathVariable("id") final String userId) {
        userService.deleteUserById(userId);
        return "redirect:/user/viewall";
    }

    /**
     * Отображение формы для регистрации пользователя.
     *
     * @param model
     * @return modelAndView
     */
    @GetMapping("/create")
    public String createForm(final Model model) {
        model.addAttribute("userForm", new UserForm());
        return "user/create";
    }

    /**
     * Создать пользователя.
     *
     * @param userForm
     * @param model
     * @return redirect
     */
    @PostMapping("/create")
    public String createUser(@ModelAttribute final UserForm userForm,
                             final Model model) {

        User user = userService.createUser(userForm);

        return "redirect:/user/viewall";
    }


    /**
     * Отображение формы для обновления информации.
     *
     * @param model
     * @param userId
     * @return modelAndView
     */
    @GetMapping("/update/{id}")
    public String updateForm(@PathVariable("id") final String userId,
                             final Model model) {

        User user = userService.getUserById(userId);

        UserForm userForm = new UserForm();
        userForm.setFirstName(user.getFirstName());
        userForm.setLastName(user.getLastName());
        userForm.setBirthDate(user.getBirthDate());
        userForm.setPhone(user.getPhone());
        userForm.setEmail(user.getEmail());
        userForm.setRole(user.getRole());

        model.addAttribute("userForm", userForm);
        model.addAttribute(USER, user);
        return "user/update";
    }

    /**
     * Update user.
     *
     * @param userForm
     * @param userId
     * @param model
     * @return model
     */
    @PostMapping("/update/{id}")
    public String updateUser(
            @ModelAttribute final UserForm userForm,
            @PathVariable("id") final String userId,
            final Model model) {

        User user = userService.getUserById(userId);

        user.setFirstName(userForm.getFirstName());
        user.setLastName(userForm.getLastName());
        user.setBirthDate(userForm.getBirthDate());
        user.setPhone(userForm.getPhone());
        user.setEmail(userForm.getEmail());
        user.setRole(userForm.getRole());

        userService.updateUser(user);

        return "redirect:/user/viewuserinfo/" + user.getUserId().toString();
    }

    /**
     * Добавить пользователя в группу.
     *
     * @param userId
     * @param group
     * @return redirect
     */
    @PostMapping("viewuserinfo/user/adduser/{id}")
    public String putUserInGroup(@PathVariable("id") final String userId, @Valid final String group) {
        User user = userService.getUserById(userId);
        userService.putUserInGroup(userId, group);

        return "redirect:/user/viewuserinfo/" + user.getUserId();
    }

}

