package team.frontend.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import team.api.group.Group;
import team.api.group.GroupForm;
import team.api.group.GroupService;
import team.api.user.User;
import team.api.user.UserService;

import java.util.List;

@RequestMapping("/group")
@Controller
public class GroupController {

    /**
     *
     */
    public static final String GROUPS_LIST = "groups";

    /**
     * Атрибут группы.
     */
    public static final String GROUP = "group";

    /**
     * Атрибут списка пользователей.
     */
    public static final String USERS = "users";
    /**
     * Ссылка на сервис пользователя.
     */
    @Autowired
    private UserService userService;

    /**
     * Ссылка на сервис групп.
     */
    @Autowired
    private GroupService groupService;

    /**
     * Сервис групп.
     *
     * @return view
     */
    @GetMapping("/")
    public String index() {

        return "group/index";
    }

    /**
     * Отображение списка всех групп.
     *
     * @param model модель для представления
     * @return view
     */
    @GetMapping("/viewallgroups")
    public String index(final Model model) {
        List<Group> list = groupService.getAllGroups();
        model.addAttribute(GROUPS_LIST, list);
        return "group/viewallgroups";
    }

    /**
     * Подробная информация о группе.
     *
     * @param groupId
     * @return model
     */
    @GetMapping("/viewgroupinfo/{id}")
    public ModelAndView view(@PathVariable("id") final String groupId) {
        ModelAndView model = new ModelAndView("group/viewgroupinfo");
        Group group = groupService.getGroupById(groupId);
        List<User> users = userService.getUsersByGroupId(groupId);
        model.addObject(USERS, users);
        model.addObject(GROUP, group);
        return model;
    }


    /**
     * Отображение формы для регистрации пользователя.
     *
     * @param model
     * @return modelAndView
     */
    @GetMapping("/creategroup")
    public String createForm(final Model model) {
        model.addAttribute("groupform", new GroupForm());
        return "group/creategroup";
    }

    /**
     * Создать пользователя.
     *
     * @param groupForm
     * @param model
     * @return redirect
     */
    @PostMapping("/creategroup")
    public String createGroup(@ModelAttribute final GroupForm groupForm,
                              final Model model) {


        Group group = groupService.createGroup(groupForm);

        return "redirect:/group/viewgroupinfo/" + group.getGroupId();
    }


    /**
     * Отображение формы для обновления информации.
     *
     * @param model
     * @param groupId
     * @return modelAndView
     */
    @GetMapping("/updategroup/{id}")
    public String updateForm(@PathVariable("id") final String groupId,
                             final Model model) {


        Group group = groupService.getGroupById(groupId);

        GroupForm groupForm = new GroupForm();
        groupForm.setGroupName(group.getGroupName());
        groupForm.setDescription(group.getDescription());

        model.addAttribute("groupForm", groupForm);
        model.addAttribute(GROUP, group);
        return "group/updategroup";
    }

    /**
     * Update user.
     *
     * @param groupForm
     * @param groupId
     * @param model
     * @return model
     */
    @PostMapping("/updategroup/{id}")
    public String updateGroup(
            @ModelAttribute final GroupForm groupForm,
            @PathVariable("id") final String groupId,
            final Model model) {

        Group group = Group.builder()
                .groupId(groupId)
                .groupName(groupForm.getGroupName())
                .description(groupForm.getDescription())
                .build();

        groupService.updateGroup(group);

        return "redirect:/group/viewgroupinfo/" + group.getGroupId();
    }

    /**
     * Удаление группы по id.
     * @param groupId
     * @return redirect viewAllGroups
     */
    @GetMapping("/deletegroup/{id}")
    public String delete(@PathVariable("id") final String groupId) {
        groupService.deleteGroupById(groupId);
        return "redirect:/group/viewallgroups";
    }
}
