package team.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import team.authorization.FrontendEntrantService;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * Зависимость на реализацию UserDetailsService.
     */
    @Autowired
    private FrontendEntrantService frontendEntrantService;

    /**
     * Параметры доступа к URL.
     *
     * @param http
     * @throws Exception
     */
    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        http
                .anonymous()
                .authorities("ROLE_ANON")
                .and()
                .authorizeHttpRequests()
                .antMatchers("/").permitAll()
                .antMatchers("/*").hasAuthority("ROLE_ANON")
                .antMatchers("/entrant/*").hasAuthority("ROLE_ANON")
                .antMatchers("/user").hasAnyAuthority("ROLE_STUDENT", "ROLE_TEACHER")
                .antMatchers("/group").hasAuthority("ROLE_TEACHER")
                .antMatchers("/user/update/*").hasAuthority("ROLE_TEACHER")
                .antMatchers("/user/create/*").hasAuthority("ROLE_TEACHER")
                .antMatchers("/group/creategroup/*").hasAuthority("ROLE_TEACHER")
                .antMatchers("/group/updategroup/*").hasAuthority("ROLE_TEACHER")
                .antMatchers("/user/deleteuser/*").hasAuthority("ROLE_TEACHER")
                .antMatchers("/group/deletegroup/*").hasAuthority("ROLE_TEACHER")
                .antMatchers("/viewuserinfo/user/adduser/*").hasAuthority("ROLE_TEACHER")
                .anyRequest().authenticated()
                .and()
                .formLogin()
                .and()
                .csrf().disable();
    }

    /**
     * Параметры доступа.
     * @param auth
     * @throws Exception
     */
    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {

        auth.userDetailsService(frontendEntrantService);

    }

}
