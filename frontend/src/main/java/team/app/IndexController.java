package team.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import team.authorization.FrontendEntrantService;

import java.util.Collection;

@Controller
public class IndexController {

    /**
     * Минимальная длина пароля.
     */
    public static final int MIN_PASS_LENGTH = 4;

    /**
     * Строковое представление роли клиента.
     */
    public static final String ROLE_STUDENT_STR = "ROLE_STUDENT";

    /**
     * Строковое представление роли менеджера.
     */
    public static final String ROLE_TEACHER_STR = "ROLE_TEACHER";

    /**
     * Объект роли клиента.
     */
    public static final SimpleGrantedAuthority ROLE_STUDENT = new SimpleGrantedAuthority(ROLE_STUDENT_STR);

    /**
     * Объект роли менеджера.
     */
    public static final SimpleGrantedAuthority ROLE_TEACHER = new SimpleGrantedAuthority(ROLE_TEACHER_STR);

    /**
     * Зависимость.
     */
    @Autowired
    private FrontendEntrantService entrantServiceDa;

    /**
     * Точка входа.
     *
     * @param authentication
     * @return index
     */

    @GetMapping("/")
    public String index(final Authentication authentication) {
        if (authentication != null && authentication.isAuthenticated()) {
            return redirectByRole(authentication.getAuthorities());
        }
        return "index";
    }

    private String redirectByRole(final Collection<? extends GrantedAuthority> authorities) {
        if (authorities.contains(ROLE_STUDENT)) {
            return "redirect:/user/";
        }
        if (authorities.contains(ROLE_TEACHER)) {
            return "redirect:/user/";
        }
        return "index";
    }

    /**
     * Форма регистрации.
     *
     * @return viewname
     */
    @GetMapping("/register")
    public String register() {
        return "register";
    }

    /**
     * Обработка формы регистрации.
     *
     * @param username
     * @param password
     * @param password2
     * @param role
     * @return redirect
     */
    @PostMapping("/register")
    public String registerProcess(
            @RequestParam(name = "username") final String username,
            @RequestParam(name = "password") final String password,
            @RequestParam(name = "password2") final String password2,
            @RequestParam(name = "role") final String role
    ) {
        String userRole = ROLE_STUDENT_STR;
        if ("TEACHER".equals(role)) {
            userRole = ROLE_TEACHER_STR;
        }

        if (!password.equals(password2) || password.length() < MIN_PASS_LENGTH) {
            return "redirect:/register?bad_password";
        }

        try {
            entrantServiceDa.insertRow(username, password, userRole);
        } catch (Exception e) {
            return "redirect:/register?invalid_request";
        }
        return "redirect:/login";
    }

}
