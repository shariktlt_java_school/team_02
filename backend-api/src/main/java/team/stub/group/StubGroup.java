package team.stub.group;

import lombok.Getter;
import lombok.Setter;
import team.api.group.Group;
import team.api.group.GroupForm;
import team.api.group.GroupService;
import team.api.user.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Getter
@Setter
public class StubGroup implements GroupService {

    /**
     * Ссылка на сервис пользователя.
     */
    private UserService userService;
    /**
     * Локальная БД Групп.
     */
    private Map<String, Group> groupDb = new HashMap<>();

    /**
     * БД групп пользователя.
     */
    private Map<String, List<Group>> userGroupsDb = new HashMap<>();

    /**
     * Получение всех доступных групп.
     *
     * @return
     */
    @Override
    public List<Group> getAllGroups() {
        return new ArrayList<>(groupDb.values());
    }

    /**
     * Получение группы по Id.
     *
     * @param groupId
     * @return
     */
    @Override
    public Group getGroupById(final String groupId) {
        return groupDb.get(groupId);
    }

    /**
     * Создание группы.
     * @param groupForm
     * @return
     */
    @Override
    public Group createGroup(final GroupForm groupForm) {
        Group group = Group.builder()
                .groupId(UUID.randomUUID().toString())
                .groupName(groupForm.getGroupName())
                .description(groupForm.getDescription())
                .build();

        if (groupDb.containsValue(group)) {
            return null;
        }

        groupDb.put(group.getGroupId(), group);
        return group;
    }

    /**
     * Удаление группы по Id.
     *
     * @param groupId
     * @return
     */
    @Override
    public String deleteGroupById(final String groupId) {
        groupDb.remove(groupId);
        return "DELETED";
    }

    /**
     * Обновление информации о группе.
     *
     * @param group
     * @return
     */
    @Override
    public Group updateGroup(final Group group) {
        return groupDb.put(group.getGroupId(), group);
    }

    /**
     * получить список групп в которых состоит пользователь.
     *
     * @param uuid
     * @return
     */
    @Override
    public List<Group> getGroupByUserId(final String uuid) {
        return userGroupsDb.get(uuid);
    }

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     * @return
     */
    @Override
    public String putUserInGroup(final String userId, final String groupId) {
        Group group = groupDb.get(groupId);

        if (!userGroupsDb.containsKey(userId)) {
            List<Group> list = new ArrayList<>();
            list.add(group);
            userGroupsDb.put(userId, list);
        }
        userGroupsDb.get(userId).add(group);
    return "DONE";
    }
}
