package team.stub.user;

import team.api.group.GroupService;
import team.api.user.User;
import team.api.user.UserForm;
import team.api.user.UserService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class StubUser implements UserService {

    /**
     * Ссылка на сервис групп.
     */
    private GroupService groupService;

    /**
     * Локальная БД пользователя.
     */
    private Map<String, User> userDb = new HashMap<>();

    /**
     * Список пользователей по группам.
     */
    private Map<String, HashSet<User>> usersInGroup = new HashMap<>();

    /**
     * Конструктор с зависимостью.
     *
     * @param bean
     */
    public StubUser(final GroupService bean) {
        groupService = bean;
    }


    /**
     * Получение пользователя по ID.
     *
     * @param userId
     * @return
     */
    @Override
    public User getUserById(final String userId) {
        return userDb.get(userId);
    }

    /**
     * Удаление пользователя по ID.
     *
     * @param userId
     */
    @Override
    public String deleteUserById(final String userId) {
        User user = userDb.get(userId);

        // Не получается придумать как удалить (
        usersInGroup.values().stream().map((s) -> {
            s.remove(user);
            return s;
        }).count();

        userDb.remove(userId);
        return "DELETED";
    }

    /**
     * Создание пользователя.
     * по умолчанию - студент.
     *
     * @param form
     * @return
     */
    @Override
    public User createUser(final UserForm form) {
        User student = User.builder()
                .userId(UUID.randomUUID().toString())
                .firstName(form.getFirstName())
                .lastName(form.getLastName())
                .birthDate(form.getBirthDate())
                .phone(form.getPhone())
                .email(form.getEmail())
                .role("Student")
                .build();

        if (userDb.containsValue(student)) {
            return null;
        }

        userDb.put(student.getUserId(), student);
        return student;
    }

    /**
     * Получение всех пользователей.
     *
     * @return
     */
    @Override
    public List<User> getAllUsers() {
        return new ArrayList<>(userDb.values());
    }

    /**
     * Обновление информации о пользователе.
     *
     * @param user
     */
    @Override
    public User updateUser(final User user) {
        if (userDb.get(user.getUserId()) == null) {
            return null;
        }

        return userDb.put(user.getUserId(), user);
    }

    /**
     * Получение списка пользователей по Id группы.
     *
     * @param groupId
     * @return
     */
    @Override
    public List<User> getUsersByGroupId(final String groupId) {
        if (usersInGroup.get(groupId) == null) {
            return new ArrayList<User>();
        }

        return new ArrayList<>(usersInGroup.get(groupId));
    }

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     */
    @Override
    public void putUserInGroup(final String userId, final String groupId) {
        User user = userDb.get(userId);

        if (!usersInGroup.containsKey(groupId)) {
            HashSet<User> userList = new HashSet<>();
            userList.add(user);
            usersInGroup.put(groupId, userList);
            return;
        }
        usersInGroup.get(groupId).add(user);
    }
}
