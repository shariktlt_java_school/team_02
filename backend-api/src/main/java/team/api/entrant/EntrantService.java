package team.api.entrant;

import team.api.common.AcceptorArgument;

public interface EntrantService {

    /**
     * Регистрация пользователя.
     *
     * @param entrantInfo
     * @return id
     */
    @AcceptorArgument
    String register(EntrantInfo entrantInfo);

    /**
     * Получение данных о пользователе.
     *
     * @param username
     * @return username
     */
    EntrantInfo loadUserByUsername(String username);
}
