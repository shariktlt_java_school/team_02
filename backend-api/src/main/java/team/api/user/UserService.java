package team.api.user;

import team.api.common.AcceptorArgument;

import java.util.List;

public interface UserService {

    /**
     * Получение пользователя по ID.
     *
     * @param userId
     * @return User
     */
    User getUserById(String userId);

    /**
     * Удаление пользователя по ID.
     *
     * @param userId
     * @return User
     */
    String deleteUserById(String userId);

    /**
     * Создание пользователя.
     * по умолчанию - студент.
     *
     * @param form
     * @return User
     */
    @AcceptorArgument
    User createUser(UserForm form);

    /**
     * Получение всех пользователей.
     *
     * @return List User
     */
    List<User> getAllUsers();

    /**
     * Обновление информации о пользователе.
     *
     * @param user
     * @return User
     */
    @AcceptorArgument
    User updateUser(User user);

    /**
     * Получение списка пользователей по Id группы.
     *
     * @param groupId
     * @return UsersList
     */

    List<User> getUsersByGroupId(String groupId);

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     */

    void putUserInGroup(String userId, String groupId);

}
