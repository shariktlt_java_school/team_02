package team.api.user;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
public class UserForm {

    /**
     * Пустой конструктор для ThymeLeaf.
     */
    public UserForm() {
    }

    /**
     * Имя.
     */
    private String firstName;

    /**
     * Фамилия.
     */
    private String lastName;

    /**
     * Дата рождения.
     */
    private String birthDate;

    /**
     * Номер телефона.
     */
    private String phone;

    /**
     * Адрес email.
     */
    private String email;

    /**
     * Роль.
     */
    private String role;

}
