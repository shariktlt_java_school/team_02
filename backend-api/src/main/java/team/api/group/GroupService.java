package team.api.group;

import team.api.common.AcceptorArgument;

import java.util.List;

public interface GroupService {

    /**
     * Получение всех доступных групп.
     *
     * @return List
     */
    List<Group> getAllGroups();

    /**
     * Получение группы по Id.
     *
     * @param groupId
     * @return Group
     */
    Group getGroupById(String groupId);

    /**
     * Создание группы.
     *
     * @param groupForm
     * @return Group
     */
    @AcceptorArgument
    Group createGroup(GroupForm groupForm);


    /**
     * Удаление группы по Id.
     *
     * @param groupId
     * @return Group
     */
    String deleteGroupById(String groupId);

    /**
     * Обновление информации о группе.
     *
     * @param group
     * @return Group
     */
    @AcceptorArgument
    Group updateGroup(Group group);

    /**
     * получить список групп в которых состоит пользователь.
     *
     * @param userId
     * @return GroupList
     */

    List<Group> getGroupByUserId(String userId);

    /**
     * Поместить пользователя в группу.
     *
     * @param userId
     * @param groupId
     * @return message
     */
    String putUserInGroup(String userId, String groupId);

}
