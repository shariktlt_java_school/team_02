package team.api.group;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.jackson.Jacksonized;

@Getter
@Setter
@Jacksonized
public class GroupForm {

    /**
     * Конструктор формы.
     */
    public GroupForm() {

    }

    /**
     * Название группы.
     */
    private String groupName;

    /**
     * Описание группы.
     */
    private String description;

}
